FROM python:3.7-slim-buster

EXPOSE 8000

RUN mkdir -p /usr/local/app
WORKDIR /usr/local/app

COPY . .

RUN ls -la
RUN echo $PWD
RUN pip install -r requirements.txt

WORKDIR /usr/local/app/doculyze
RUN echo $PWD
RUN ls -la

RUN python manage.py migrate
RUN python -m nltk.downloader punkt

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]