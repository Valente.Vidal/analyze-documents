# Doculyze

Doculyze is a web app that analyzes a single or multiple files and returns the 10 most common words from all files. It also quotes the sentence containing the word and the file where you can find that sentence. This project only works with English files.

### Tech Stack

  - Language: [Python] 3.7
  - Web Framework: [Django]
  - Natural Language Processor: [NLTK]
  - Container Type: [Docker]

You can:
  - You can modify the StopWords in /web/stopwords.txt
  - Test it using the book "A Planet for you Thoughts" and "Pride and Prejudice" in the repo

### Installation

If in an enviorment running Docker:
```sh
$ git clone git@gitlab.com:ValenteFV/analyze-documents.git
$ cd analyze-documents
$ bash run.sh
```
once its running you can visit http://127.0.0.1:8000/

To stop and remove the container run the following:
```sh
$ docker stop docu
$ docker rm docu
```
if you wish a live demo, contact me or visit my Terraform repos

### Todos

 - Rework the logic behind counting the words
 - Work on the front end

This README was made with [Dillinger]
**Free Software, Hell Yeah!**

   [Docker]: <https://www.docker.com/>
   [Python]: <https://www.python.org/downloads/release/python-370/>
   [NLTK]: <https://www.nltk.org/>
   [Django]: <https://www.djangoproject.com/>
   [Dillinger]: <https://dillinger.io/>

