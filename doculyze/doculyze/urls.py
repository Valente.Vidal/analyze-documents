
from django.contrib import admin
from django.urls import path, re_path
from web import views as web_views

from django.shortcuts import render
from django.http import JsonResponse
from django.views import View
from web.views import *

from django.contrib.staticfiles.views import serve as serve_static

def _static_butler(request, path, **kwargs):
    """
    Serve static files using the django static files configuration
    WITHOUT collectstatic. This is slower, but very useful for API 
    only servers where the static files are really just for /admin

    Passing insecure=True allows serve_static to process, and ignores
    the DEBUG=False setting
    """
    return serve_static(request, path, insecure=True, **kwargs)





urlpatterns = [
    path('admin/', admin.site.urls),
    path('', web_views.home, name='home'),
    path('basic-upload/', BasicUploadView.as_view(),  name='basic_upload'),
    path('analyze-docs/', AnalyzeView.as_view(), name='analyze'),
    re_path(r'static/(.+)', _static_butler),
]
